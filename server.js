/*requiring node modules starts */

//var app = require("express")();
var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require("socket.io")(http);
var Session = require('express-session');
var cookieParser = require('cookie-parser'); 
var path = require('path');
/*requiring node modules ends */


// the session is stored in a cookie, so we use this to parse it
app.use(cookieParser());

var Session= Session({
	secret:'secrettokenhere',
	saveUninitialized: true,
	resave: true
});


io.use(function(socket, next) {
	    Session(socket.request, socket.request.res, next);
});


app.use(Session);

var sessionInfo;

/* requiring config file starts*/
var config =require('./middleware/config.js')(app);
/* requiring config file ends*/

/* requiring config db.js file starts*/
var db = require("./middleware/db.js");
var connection_object= new db();
var connection=connection_object.connection; // getting conncetion object here 
/* requiring config db.js file ends*/


/* 
	1. Requiring auth-routes.js file, which takes care of all Login & Registration page operation.
	2. Passing object of express, Database connection, expressSession and cookieParser.
	3. auth-routes.js contains the methods and routes for Login and registration page. 
*/
require('./middleware/auth-routes.js')(app,connection,Session,cookieParser,sessionInfo);
/* 
	1. Requiring routes.js file, which takes handles the Home page operation.
	2. Passing object of express, Database connection and object of socket.io as 'io'.
	3. routes.js contains the methods and routes for Home page  
*/
require('./middleware/routes.js')(app,connection,io,Session,cookieParser,sessionInfo);
	
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.bodyParser());
var helper = require('./middleware/helper');
exports.helper = helper

app.get('/data', function(req, res){
	var data = req.body;
	helper.getEvents(data,connection,function(result){
		if(result){
		for (var i = 0; i < result.length; i++)
		 var objs = [];
		 for (var i = 0;i < result.length; i++) {
			 objs.push({id: result[i]._id, 
				start_date: result[i].start_date,
				end_date: result[i].end_date,
				text: result[i].text});
		 }
		 //console.log(JSON.stringify(objs));
	res.send(JSON.stringify(objs));
	}else
		res.send(JSON.stringify([]));
	});
});

app.post('/data', function(req, res){
	var data = req.body;
	var mode = data["!nativeeditor_status"];
	var sid = data.id;
	var tid = sid;

	delete data.id;
	delete data.gr_id;
	delete data["!nativeeditor_status"];

	function update_response(err, result){
		if (err)
			mode = "error";
		else if (mode == "inserted")
			tid = data._id;

		res.setHeader("Content-Type","text/xml");
		console.log("<data><action type='"+mode+"' sid='"+sid+"' tid='"+tid+"'/></data>");
		res.send("<data><action type='"+mode+"' sid='"+sid+"' tid='"+tid+"'/></data>");
	}

	if (mode == "updated"){		
		helper.updateCalerdarEvent(sid, data, connection, function(result){
			console.log("updated");
		});
	}
	else if (mode == "inserted"){
		console.log(data);
		helper.insertCalerdarEvent(data,connection,function(result){
			console.log(result);
		});
	}
	else if (mode == "deleted"){
		console.log(sid);
		helper.deleteCalerdarEvent(sid, connection, function(result){
			console.log(result);
		});
	}
	else{
		res.send("Not supported operation");
	}
});

/*
	Running our application  
*/
http.listen(81,function(){
    console.log("Listening on http://127.0.0.1:81");
});